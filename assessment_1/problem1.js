const fs = require('fs');

//callback for checking creating files
let callback1  = (err) => {
    if(err){
        console.log(err)
    } else{
        console.log('Random files created successfully');
    };
};

// callback for checking files deletion
let callback2  = (err) => {
    if(err){
        console.log(err);
    } else{
        console.log('Random files deleted successfully');
    };
};

// main function
function creatingAndDeletingJsonFiles(data){
    fs.mkdir('./temp', (err) => {
        if(err){
            console.log(err);
            return;
        } else{
            let count = 0;
            for(let index=1; index<5; index++){
                fs.writeFile(`./temp/details${index}.json`, JSON.stringify(data[index]), err => {
                    if (err) {
                        console.log(err);
                    } else {
                        count++
                        console.log('Successfully wrote file details');
                    };

                    if(count === 4){
                        callback1(err);
                    };
                });
            };
            let deleteCount = 0;
            for (let index = 1; index<5; index++){
                fs.rm(`./temp/details${index}.json`, (err) => {
                    if(err){
                        console.error(err);
                        return;
                    }else{
                        deleteCount++
                        console.log('File deleted successfully details');
                    };

                    if(deleteCount === 4){
                        callback2(err)
                    };
                });
            };
        };
    });
};

module.exports = creatingAndDeletingJsonFiles;