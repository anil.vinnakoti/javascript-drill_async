const fs = require('fs');

function problem2(filePath){
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if(err){
            console.log(err);
        } else {
            fs.writeFile('./upperCase.txt', data.toUpperCase(), (err) =>{
                if(err){
                    console.log(err);
                } else{
                    console.log('Written a new file with upper case content');
                    fs.writeFile('./filenames.txt', 'upperCase.txt\n', (err) => {
                        if(err){
                            console.log(err);
                        } else{
                            console.log('Successfully stored the new file filename upperCase.txt in filenames.txt');
                            fs.readFile('./upperCase.txt', 'utf-8', (err, data) => {
                                if(err){
                                    console.log(err);
                                } else {
                                    fs.writeFile('./lowerCaseSentences.txt', data.toLowerCase().split('.').join('\n'), (err) =>{
                                        if(err){
                                            console.log(err);
                                        } else{
                                            console.log('Written a new file with lower case content');
                                            fs.appendFile('./filenames.txt', 'lowerCaseSentences.txt\n', (err) => {
                                                if(err){
                                                    console.log(err);
                                                } else{
                                                    console.log('Successfully stored the new file filename upperCase.txt in filenames.txt');
                                                    fs.readFile('./lowerCaseSentences.txt', 'utf-8', (err, data) => {
                                                        if(err){
                                                            console.log(err);
                                                        } else{
                                                            fs.writeFile('./sortedSentences.txt', data.split('\n').sort().join('\n'), (err) =>{
                                                                if(err){
                                                                    console.log(err);
                                                                } else{
                                                                    fs.appendFile('./filenames.txt', 'sortedSentences.txt\n', (err) => {
                                                                        if(err){
                                                                            console.log(err);
                                                                        } else{
                                                                            console.log('Successfully stored the new file filename sortedSentences.txt in filenames.txt');
                                                                            fs.readFile('./filenames.txt', 'utf-8', (err, data) =>{
                                                                                if(err){
                                                                                    console.log(err);
                                                                                } else{
                                                                                    const filenamesArray = data.split('\n');
                                                                                    for(let index=0; index<filenamesArray.length -1; index++){
                                                                                        fs.rm(`${filenamesArray[index]}`, (err) => {
                                                                                            if(err){
                                                                                                console.error(err);
                                                                                                return;
                                                                                            } else{
                                                                                                console.log("File deleted successfully");
                                                                                            };
                                                                                        });
                                                                                    };
                                                                                };
                                                                            });
                                                                        };
                                                                    });
                                                                };
                                                            });
                                                        };
                                                    });
                                                };
                                            });
                                        };
                                    });
                                };
                            });
                        };
                    });
                };

            });
        };
    });
};

module.exports = problem2;