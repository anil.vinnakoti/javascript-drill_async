/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID 
    that is passed to it from the given data in cards.json. Then pass control back to the code 
    that called it by using a callback function.
*/

function cardsWithListID(id, data, callback){
    setTimeout(() => {
        if(data[id]){
            callback(null, data[id]);
        } else{
            callback(new Error(`card informatioon not found with id ${id}`));
        }
    }, 2 * 1000)
};

module.exports = cardsWithListID;