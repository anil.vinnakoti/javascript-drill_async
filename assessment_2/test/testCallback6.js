const callback6 = require('../callback6');

const boardFn = require('../callback1');
const listFn = require('../callback2');
const cardFn = require('../callback3');

const boardData = require('../boards.json');
const listData = require('../lists.json');
const cardData = require('../cards.json');

callback6(boardFn, listFn, cardFn, boardData, listData, cardData);