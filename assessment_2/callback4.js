/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/



function callback4(boardFn, listFn, cardFn, boardData, listData, cardData){
    setTimeout(() =>{
        boardFn('mcu453ed', boardData, (err,boardResult) =>{
            if(err){
                console.log(err);
            } else{
                console.log(boardResult);

                const ThanosID = boardResult['id'];
                listFn(ThanosID, listData, (error, listResult) =>{
                    if(error){
                        console.log(error);
                    } else{
                        console.log(listResult);

                        const indexOfMind = listResult.findIndex((object) => object.name === 'Mind');
                        const mindID = listResult[indexOfMind]['id'];

                        cardFn(mindID, cardData, (error, cardResult) => {
                            if(error){
                                console.log(error);
                            } else{
                                console.log(cardResult);
                            };
                        })
                    };
                })
            }
        });

    }, 2 * 1000)
};

module.exports = callback4;