/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

function callback5(boardFn, listFn, cardFn, boardData, listData, cardData){
    setTimeout(() =>{
        boardFn('mcu453ed', boardData, (err,boardResult) =>{
            if(err){
                console.log(err);
            } else{
                console.log(boardResult);

                const ThanosID = boardResult['id'];
                listFn(ThanosID, listData, (error, listResult) =>{
                    if(error){
                        console.log(error);
                    } else{
                        console.log(listResult);

                        const indexOfMind = listResult.findIndex((object) => object.name === 'Mind');
                        const indexOfSpace = listResult.findIndex((object) => object.name === 'Space');
                        
                        const mindID = listResult[indexOfMind]['id'];
                        const spaceID = listResult[indexOfSpace]['id'];

                        cardFn(mindID, cardData, (error, mindCards) => {
                            if(error){
                                console.log(error);
                            } else{
                                console.log(mindCards);
                            };
                        });
                        cardFn(spaceID, cardData, (error, spaceCards) => {
                            if(error){
                                console.log(error);
                            } else{
                                console.log(spaceCards);
                            };
                        })
                    };
                })
            }
        });

    }, 2 * 1000)
};

module.exports = callback5;