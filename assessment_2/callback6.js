/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

function callback6(boardFn, listFn, cardFn, boardData, listData, cardData){
    setTimeout(() => {
        boardFn('mcu453ed', boardData, (err,boardResult) =>{
            if(err){
                console.log(err);
            } else{
                console.log(boardResult);

                const ThanosID = boardResult['id'];
                listFn(ThanosID, listData, (error, listResult) =>{
                    if(error){
                        console.log(error);
                    } else{
                        console.log(listResult);

                        for(let index = 0; index<listResult.length;index++){
                            const cardID = listResult[index]['id'];

                            cardFn(cardID, cardData, (error, cardInfo) => {
                                if(cardInfo){
                                    console.log(cardInfo);
                                } else{
                                    console.log(error.message);
                                }
                            })
                        }
                    };
                })
            }
        });
    }, 2 * 1000);
};

module.exports = callback6;