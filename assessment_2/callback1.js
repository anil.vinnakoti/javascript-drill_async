/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID 
    that is passed from the given list of boards in boards.json and then pass control back to the code 
    that called it by using a callback function.
*/

function findBoardInfo(id, data,callback){
    setTimeout(() =>{
        const idInData = data.find((object) => object.id === id);
        
        if(idInData){
            callback(null, idInData);
        } else{
            callback(new Error(`Board informatioon not found with id ${id}`));
        }
    }, 2 * 1000);
};

module.exports = findBoardInfo;